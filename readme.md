# Enigma

Designed to enable the simple encoding/decoding of messages using a software version of the World War II Enigma machine.

## Getting Started

Install Python 3
Create an Enigma machine and choose its reflector and rotors
```python
from src.machines import Enigma
from src.enigma_reflectors import B
from src.enigma_rotors import I, II, III

enigma = Enigma(reflector=B, rotors=[I, II, III])
```
Set the rotor positions, settings and plugboard pairs (if desired)
```python
enigma.set_start_position('ABL')
enigma.set_rotor_settings('XMV')
enigma.set_plugs('AM FI NV PS TU WZ')
```
Enter the message to encode/decode
```python
message = 'This is a secret message'
encoded = enigma.enter(message)
print(encoded)
```
Produces:
> WCSJ DF K BNUPXR XFLJKEV

## Tests

The tests are split into 'small', 'medium' and 'large' (similar to unit, integration and acceptance tests respectively).  
Small tests are used to test simple, isolated classes and utilities.  
Medium tests are used to test classes that require other objects to be setup in order to run correctly.  
Large tests are used to ensure the system as a whole runs in the expected manner.

Tests can be executed by running `python -m unittest discover tests/` from the root directory.
