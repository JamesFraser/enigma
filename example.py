"""An example to show how to use Enigma in this project. Any settings can be changed."""
from src.machines import Enigma
from src.enigma_reflectors import B
from src.enigma_rotors import I, II, III

enigma = Enigma(reflector=B, rotors=[I, II, III])

enigma.set_start_position('ABL')
enigma.set_rotor_settings('XMV')
enigma.set_plugs('AM FI NV PS TU WZ')

message = 'This is a secret message'
encoded = enigma.enter(message)
print(encoded)
