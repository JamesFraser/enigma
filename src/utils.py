ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def char_to_int(letter):
    return ord(letter.upper()) - 65


def int_to_char(integer):
    return chr(integer + 65)
