"""
Used to represent the main components used in encoding/decoding an enigma message
Contains:
    Reflector - used for one way mapping of input
    Rotor - used for two way mapping of input and handling mechanical logic
"""
from src.utils import char_to_int


class Reflector:
    """
    Represent a reflector, used for one way mapping.

    Attributes:
        mapping (dict): Represents the wiring used to map one plate contact to another.
    """

    def __init__(self, mapping):
        self.mapping = {i: char_to_int(mapping[i]) for i in range(26)}

    def reflect(self, char_int):
        """
        Return the index of the plate contact wired to the input.

        :param char_int: The index of the input plate contact.
        :return: The index of the connected plate contact.
        """
        return self.mapping[char_int]


class Rotor:
    """
    Represent a rotor, used for two way mapping.

    Attributes:
        notches (list): The letters which cause the next rotor to rotate when reached.
        offset (int): How many places the rotor has rotated from the 'A' position.
            (Also known as Grundstellung)
        setting_offset (int): How many places the internal wiring has been offset.
            (Also known as Ringstellung)
        left (dict): How the wires connect plate contacts when moving in a left direction.
        right (dict): How the wires connect plate contacts when moving in a right direction.
    """

    def __init__(self, notches, mapping):
        self.notches = [char_to_int(notch) for notch in notches]
        self.offset = 0
        self.setting_offset = 0
        self.left = {i: char_to_int(mapping[i]) for i in range(26)}
        self.right = {char_to_int(mapping[i]): i for i in range(26)}

    def set_start_position(self, start_position):
        """
        Set the rotor to the letter specified.

        :param start_position: The letter to set this rotor to.
        """
        self.offset = char_to_int(start_position)

    def set_setting(self, setting):
        """
        Set the rotor internal setting to the letter specified.

        :param setting: The letter to set this rotor's internal setting to.
        """
        self.setting_offset = char_to_int(setting)

    def rotate(self):
        """Rotate this rotor by one place."""
        self.offset = (self.offset + 1) % 26

    def map_left(self, char_int):
        """
        Return the index of the plate contact wired to the input when moving left.

        :param char_int: The index of the input plate contact.
        :return: The index of the connected plate contact.
        """
        total_offset = self.offset - self.setting_offset
        index = (char_int + total_offset) % 26
        return (self.left[index] - total_offset) % 26

    def map_right(self, char_int):
        """
        Return the index of the plate contact wired to the input when moving right.

        :param char_int: The index of the input plate contact.
        :return: The index of the connected plate contact.
        """
        total_offset = self.offset - self.setting_offset
        index = (char_int + total_offset) % 26
        return (self.right[index] - total_offset) % 26

    @property
    def notched(self):
        """
        Return whether the rotor is in a notched position.

        :return: True if the rotor is in a notched position, false if not.
        """
        return self.offset in self.notches
