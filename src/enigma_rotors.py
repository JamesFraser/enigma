"""
Represents the rotors used in various Enigma models.
Contains:
    The letter(s) which causes the next rotor to rotate.
    The mapping that this rotor uses to map input to output.
"""
from src.components import Rotor


I = Rotor(notches=['Q'], mapping='EKMFLGDQVZNTOWYHXUSPAIBRCJ')
II = Rotor(notches=['E'], mapping='AJDKSIRUXBLHWTMCQGZNPYFVOE')
III = Rotor(notches=['V'], mapping='BDFHJLCPRTXVZNYEIWGAKMUSQO')
IV = Rotor(notches=['J'], mapping='ESOVPZJAYQUIRHXLNFTGKDCMWB')
V = Rotor(notches=['Z'], mapping='VZBRGITYUPSDNHLXAWMJQOFECK')
VI = Rotor(notches=['Z', 'M'], mapping='JPGVOUMFYQBENHZRDKASXLICTW')
VII = Rotor(notches=['Z', 'M'], mapping='NZJHGRCXMYSWBOUFAIVLPEKQDT')
VIII = Rotor(notches=['Z', 'M'], mapping='FKQHTLXOCBJSPDZRAMEWNIUYGV')
