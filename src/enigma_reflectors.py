"""
Represents the reflectors used in various Enigma models.
Contains:
    The mapping that this reflector uses to map input to output.
"""
from src.components import Reflector

BETA = Reflector(mapping='LEYJVCNIXWPBQMDRTAKZGFUHOS')
GAMMA = Reflector(mapping='FSOKANUERHMBTIYCWLQPZXVGJD')
A = Reflector(mapping='EJMZALYXVBWFCRQUONTSPIKHGD')
B = Reflector(mapping='YRUHQSLDPXNGOKMIEBFZCWVJAT')
C = Reflector(mapping='FVPJIAOYEDRZXWGCTKUQSBNMHL')
B_THIN = Reflector(mapping='ENKQAUYWJICOPBLMDXZVFTHRGS')
C_THIN = Reflector(mapping='RDOBJNTKVEHMLFCWZAXGYIPSUQ')
ETW = Reflector(mapping='ABCDEFGHIJKLMNOPQRSTUVWXYZ')
