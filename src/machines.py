"""Contains implementation for the Enigma machine"""
from copy import copy

from src.utils import char_to_int, int_to_char, ALPHABET


class Enigma:
    """
    Represent the Enigma machine.

    Attributes:
        rotors (list): The rotors to be used (ordered left to right).
        reflector (list): The reflector to be used at the left-hand side.
        plugs (dict): The mappings of keys on the enigma keyboard.
            (each key maps to their own letter by default)
    """

    def __init__(self, reflector, rotors):
        self.rotors = [copy(rotor) for rotor in rotors]
        self.reflector = copy(reflector)
        self.plugs = {char: char for char in ALPHABET}

    def set_start_position(self, settings_string):
        """
        Set the rotors to the letters specified.

        :param settings_string: The letters to set the rotors to.
        """
        for rotor, char in zip(self.rotors, settings_string):
            rotor.set_start_position(char)

    def set_rotor_settings(self, settings_string):
        """
        Set the rotors' internal settings to the letters specified.

        :param settings_string: The letters to set the rotors' internal setting to.
        """
        for rotor, char in zip(self.rotors, settings_string):
            rotor.set_setting(char)

    def set_plugs(self, pairs_string):
        """
        Map pairs of keys to use each other's value on input and output.

        Example:
            set_plugs('AB CD') - will swap 'A' with 'B' and 'C' with 'D'

        :param pairs_string: A string listing each pair of keys
        """
        self.plugs = {char: char for char in ALPHABET}
        pairs = pairs_string.upper().split(' ')
        for left, right in pairs:
            self.plugs[left] = right
            self.plugs[right] = left

    def enter(self, string):
        """
        Enter a message into the machine.

        :param string: The input string to be encoded/decoded.
        :return: The encoded/decoded string.
        """
        string = string.upper()
        return ''.join([self._enter_char(c) for c in string])

    def _enter_char(self, char):
        if char not in ALPHABET:
            return char

        self._rotate_rotors()
        char = self.plugs[char]
        plate_contact = char_to_int(char)

        for rotor in self.rotors[::-1]:
            plate_contact = rotor.map_left(plate_contact)

        plate_contact = self.reflector.reflect(plate_contact)

        for rotor in self.rotors:
            plate_contact = rotor.map_right(plate_contact)

        char = int_to_char(plate_contact)
        return self.plugs[char]

    def _rotate_rotors(self):
        left, middle, right = self.rotors
        if middle.notched:
            left.rotate()
        if right.notched or middle.notched:
            middle.rotate()
        right.rotate()

    @property
    def rotors_string(self):
        return ''.join(int_to_char(rotor.offset) for rotor in self.rotors)
