from unittest import TestCase

from src.components import Reflector, Rotor
from src.utils import ALPHABET


class TestReflector(TestCase):

    def test_reflect_first(self):
        reflector = Reflector(ALPHABET[::-1])
        reflected = reflector.reflect(0)
        self.assertEqual(25, reflected)

    def test_reflect_last(self):
        reflector = Reflector(ALPHABET[::-1])
        reflected = reflector.reflect(25)
        self.assertEqual(0, reflected)


class TestRotor(TestCase):

    def test_set_start_position_first(self):
        rotor = Rotor('A', ALPHABET)
        rotor.set_start_position('A')
        self.assertEqual(0, rotor.offset)

    def test_set_start_position_last(self):
        rotor = Rotor('A', ALPHABET)
        rotor.set_start_position('Z')
        self.assertEqual(25, rotor.offset)

    def test_set_start_position_accepts_lower_case(self):
        rotor = Rotor('A', ALPHABET)
        rotor.set_start_position('a')
        self.assertEqual(0, rotor.offset)

    def test_rotate(self):
        rotor = Rotor('A', ALPHABET)
        rotor.set_start_position('A')
        rotor.rotate()
        self.assertEqual(1, rotor.offset)

    def test_rotate_cycles(self):
        rotor = Rotor('A', ALPHABET)
        rotor.set_start_position('Z')
        rotor.rotate()
        self.assertEqual(0, rotor.offset)
