from unittest import TestCase

from src.utils import char_to_int, int_to_char


class TestUtils(TestCase):

    def test_char_to_int_a(self):
        self.assertEqual(0, char_to_int('A'))

    def test_char_to_int_z(self):
        self.assertEqual(25, char_to_int('Z'))

    def test_char_to_int_a_lowercase(self):
        self.assertEqual(0, char_to_int('a'))

    def test_int_to_char_0(self):
        self.assertEqual('A', int_to_char(0))

    def test_int_to_char_25(self):
        self.assertEqual('Z', int_to_char(25))
