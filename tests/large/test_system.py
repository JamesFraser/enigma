from unittest import TestCase

from src.enigma_reflectors import A, B, C
from src.enigma_rotors import I, II, III, IV, V, VI, VII, VIII
from src.machines import Enigma


class TestSystem(TestCase):

    def test_madlab_example_one_standard_setup(self):
        """Taken from http://www.madlab.org/guides/enigma.html"""
        enigma = Enigma(reflector=B, rotors=[I, II, III])

        plaintext = 'OPGN DXCF WEVT NRSD ULTP'
        expected = 'THIS ISAS ECRE TMES SAGE'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_madlab_example_one_custom_rotors(self):
        """Taken from http://www.madlab.org/guides/enigma.html"""
        enigma = Enigma(reflector=C, rotors=[VII, I, III])

        plaintext = 'ZUZB PCBG EOGY TRPB VUXG QTIX AWHT ZDZV ITOA'
        expected = 'ENIG MAWA SUSE DDUR INGT HESE COND WORL DWAR'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_mlb_example_with_start_position_and_plugboard(self):
        """Taken from http://www.mlb.co.jp/linux/science/genigma/enigma-referat/node4.html"""
        enigma = Enigma(reflector=B, rotors=[II, I, V])
        enigma.set_start_position('FRA')
        enigma.set_plugs('AB IR UX KP')

        plaintext = 'PCDAONONEBCJBOGLYMEEYGSHRYUBUJHMJOQZLEX'
        expected = 'ANBULMEGRAZGOESTINGSTRENGGEHEIMEMELDUNG'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_enigma_manual_example(self):
        """Taken from http://cryptocellar.org/Enigma/EMsg1930.html"""
        enigma = Enigma(reflector=A, rotors=[II, I, III])
        enigma.set_start_position('ABL')
        enigma.set_rotor_settings('XMV')
        enigma.set_plugs('AM FI NV PS TU WZ')

        plaintext = 'GCDSE AHUGW TQGRK VLFGX UCALX VYMIG MMNMF DXTGN VHVRM MEVOU YFZSL RHDRR XFJWC FHUHM UNZEF RDISI KBGPM YVXUZ'
        expected = 'FEIND LIQEI NFANT ERIEK OLONN EBEOB AQTET XANFA NGSUE DAUSG ANGBA ERWAL DEXEN DEDRE IKMOS TWAER TSNEU STADT'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_enigma_barbarossa_one(self):
        """Taken from http://wiki.franklinheath.co.uk/index.php/Enigma/Sample_Messages"""
        enigma = Enigma(reflector=B, rotors=[II, IV, V])
        enigma.set_start_position('BLA')
        enigma.set_rotor_settings('BUL')
        enigma.set_plugs('AV BS CG DL FU HZ IN KM OW RX')

        plaintext = 'EDPUD NRGYS ZRCXN UYTPO MRMBO FKTBZ REZKM LXLVE FGUEY SIOZV EQMIK UBPMM YLKLT TDEIS MDICA GYKUA CTCDO MOHWX MUUIA UBSTS LRNBZ SZWNR FXWFY SSXJZ VIJHI DISHP RKLKA YUPAD TXQSP INQMA TLPIF SVKDA SCTAC DPBOP VHJK-'
        expected = 'AUFKL XABTE ILUNG XVONX KURTI NOWAX KURTI NOWAX NORDW ESTLX SEBEZ XSEBE ZXUAF FLIEG ERSTR ASZER IQTUN GXDUB ROWKI XDUBR OWKIX OPOTS CHKAX OPOTS CHKAX UMXEI NSAQT DREIN ULLXU HRANG ETRET ENXAN GRIFF XINFX RGTX-'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_enigma_barbarossa_two(self):
        """Taken from http://wiki.franklinheath.co.uk/index.php/Enigma/Sample_Messages"""
        enigma = Enigma(reflector=B, rotors=[II, IV, V])
        enigma.set_start_position('LSD')
        enigma.set_rotor_settings('BUL')
        enigma.set_plugs('AV BS CG DL FU HZ IN KM OW RX')

        plaintext = 'SFBWD NJUSE GQOBH KRTAR EEZMW KPPRB XOHDR OEQGB BGTQV PGVKB VVGBI MHUSZ YDAJQ IROAX SSSNR EHYGG RPISE ZBOVM QIEMM ZCYSG QDGRE RVBIL EKXYQ IRGIR QNRDN VRXCY YTNJR'
        expected = 'DREIG EHTLA NGSAM ABERS IQERV ORWAE RTSXE INSSI EBENN ULLSE QSXUH RXROE MXEIN SXINF RGTXD REIXA UFFLI EGERS TRASZ EMITA NFANG XEINS SEQSX KMXKM XOSTW XKAME NECXK'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_enigma_scharnhorst(self):
        """Taken from http://wiki.franklinheath.co.uk/index.php/Enigma/Sample_Messages"""
        enigma = Enigma(reflector=B, rotors=[III, VI, VIII])
        enigma.set_start_position('UZV')
        enigma.set_rotor_settings('AHM')
        enigma.set_plugs('AN EZ HK IJ LR MQ OT PV SW UX')

        plaintext = 'YKAE NZAP MSCH ZBFO CUVM RMDP YCOF HADZ IZME FXTH FLOL PZLF GGBO TGOX GRET DWTJ IQHL MXVJ WKZU ASTR'
        expected = 'STEU EREJ TANA FJOR DJAN STAN DORT QUAA ACCC VIER NEUN NEUN ZWOF AHRT ZWON ULSM XXSC HARN HORS THCO'
        actual = enigma.enter(plaintext)
        
        self.assertEqual(expected, actual)

    def test_two_machines_do_not_share_components(self):
        enigma1 = Enigma(reflector=B, rotors=[I, II, III])
        enigma2 = Enigma(reflector=B, rotors=[I, II, III])

        plaintext = 'OPGN DXCF WEVT NRSD ULTP'
        expected = 'THIS ISAS ECRE TMES SAGE'
        actual1 = enigma1.enter(plaintext)
        actual2 = enigma2.enter(plaintext)

        self.assertEqual(expected, actual1)
        self.assertEqual(expected, actual2)

    def test_enigma_handles_lower_case_input(self):
        enigma = Enigma(reflector=B, rotors=[I, II, III])

        plaintext = 'opgn dxcf wevt nrsd ultp'
        expected = 'THIS ISAS ECRE TMES SAGE'
        actual = enigma.enter(plaintext)

        self.assertEqual(expected, actual)
