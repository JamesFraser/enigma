from unittest import TestCase

from src.enigma_reflectors import B
from src.enigma_rotors import I, II, III
from src.machines import Enigma


class TestEnigma(TestCase):

    def test_normal_step_sequence(self):
        enigma = Enigma(reflector=B, rotors=[I, II, III])
        enigma.set_start_position('AAU')
        enigma.enter('AAA')
        self.assertEqual('ABX', enigma.rotors_string)

    def test_double_step_sequence(self):
        enigma = Enigma(reflector=B, rotors=[I, II, III])
        enigma.set_start_position('ADU')
        enigma.enter('AAAA')
        self.assertEqual('BFY', enigma.rotors_string)
